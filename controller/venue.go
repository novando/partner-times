package controller

import (
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type venue struct {
	serv service.VenueService
}

func NewVenueController(sv service.VenueService) *venue {
	return &venue{serv: sv}
}

func (c *venue) GetAll(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "VENUES_FOUND"
	var venueData []dto.Venue
	var count int64
	if token["roleId"].(float64) <= 2 {
		venueData, count = c.serv.GetAll(pagingPage, pagingSize)
		if count == 0 {
			msgVal = "DATA_NOT_FOUND"
		}
	} else {
		companyId := uint(token["companyId"].(float64))
		venueData, count = c.serv.GetByCompany(companyId, pagingPage, pagingSize)
		if count == 0 {
			msgVal = "DATA_NOT_FOUND"
		}
	}
	res := arutek.Response(msgVal, venueData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *venue) GetByCompany(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	companyId, err := strconv.Atoi(ctx.Param("companyId"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "VENUES_FOUND"
	venueData, count := c.serv.GetByCompany(uint(companyId), pagingPage, pagingSize)
	if count == 0 {
		msgVal = "DATA_NOT_FOUND"
	}
	res := arutek.Response(msgVal, venueData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *venue) GetDetail(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId := ctx.Param("id")
	venueId, err := strconv.Atoi(paramId)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "VENUE_FOUND"
	venueData := c.serv.GetDetail(uint(venueId))
	res := arutek.Response(msgVal, venueData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *venue) Create(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var venueDto dto.VenueReq
	err = ctx.ShouldBindJSON(&venueDto)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "VENUE_CREATED"
	err = c.serv.Create(venueDto, uint(token["companyId"].(float64)))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response(msgVal, nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *venue) Update(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var venueDto dto.VenueReq
	err = ctx.ShouldBindJSON(&venueDto)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId := ctx.Param("id")
	venueId, err := strconv.Atoi(paramId)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "VENUE_CREATED"
	err = c.serv.Update(uint(venueId), venueDto)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response(msgVal, nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *venue) Delete(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId := ctx.Param("id")
	venueId, err := strconv.Atoi(paramId)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	err = c.serv.Delete(uint(venueId))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "VENUE_DELETED"
	res := arutek.Response(msgVal, nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
