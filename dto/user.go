package dto

type RegisterReq struct {
	Name     string `json:"name" binding:"required"`
	Email    string `json:"email" binding:"required,email"`
	RoleID   uint   `json:"roleId"`
	Password string `json:"password" binding:"required,min=8"`
	ConfPass string `json:"confPass" binding:"required,min=8"`
}

type LoginReq struct {
	Email    string `json:"email" form:"email" binding:"required,email"`
	Password string `json:"password" binding:"required"`
}

type UserRes struct {
	Name        string `json:"name"`
	Email       string `json:"email"`
	RoleName    string `json:"roleName"`
	CompanyName string `json:"companyName"`
	CompanyLogo string `json:"companyLogo"`
}
