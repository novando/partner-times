package repository

import (
	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type VariantRepository interface {
	Save(variant *model.Variant) error
	CountByPackages(packageIds []uint) (count int64)
	GetByPackages(packageIds []uint, page int, size int) []model.Variant
	CountByCompany(companyId uint) (count int64)
	GetByCompany(companyId uint, page int, size int) []model.Variant
	GetById(id uint) model.Variant
	Delete(id uint) error
}

type variant struct {
	db *gorm.DB
}

func NewVariantRepository(db *gorm.DB) VariantRepository {
	return &variant{db: db}
}

func (r *variant) Save(variant *model.Variant) error {
	return r.db.Save(&variant).Error
}

func (r *variant) CountByPackages(packageIds []uint) (count int64) {
	r.db.Table("variants").Where("package_id IN ?", packageIds).Count(&count)
	return
}

func (r *variant) GetByPackages(packageIds []uint, page int, size int) []model.Variant {
	var variantData []model.Variant
	r.db.Preload("Package").Where("package_id IN ?", packageIds).Scopes(arutek.GormPaginate(page, size)).Find(&variantData)
	return variantData
}

func (r *variant) CountByCompany(companyId uint) (count int64) {
	r.db.Table("variants").Where("package_id IN (SELECT id FROM packages WHERE company_id = ?", companyId).Count(&count)
	return
}

func (r *variant) GetByCompany(companyId uint, page int, size int) []model.Variant {
	var variantData []model.Variant
	r.db.Preload("Package").Where("package_id IN (SELECT id FROM packages WHERE company_id = ?)", companyId).Scopes(arutek.GormPaginate(page, size)).Find(&variantData)
	return variantData
}

func (r *variant) GetById(id uint) model.Variant {
	var variantData model.Variant
	r.db.Preload("Package").First(&variantData, id)
	return variantData
}

func (r *variant) Delete(id uint) error {
	return r.db.Delete(&model.Variant{}, id).Error
}
