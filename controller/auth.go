package controller

import (
	"net/http"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type auth struct {
	serv service.UserService
}

func NewAuthController(userService service.UserService) *auth {
	return &auth{serv: userService}
}

func (c *auth) Login(ctx *gin.Context) {
	var reqBody dto.LoginReq
	err := ctx.ShouldBind(&reqBody)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	token, err, statusCode := c.serv.Login(reqBody.Email, reqBody.Password)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("AUTHORIZED", token, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *auth) Register(ctx *gin.Context) {
	var reqBody dto.RegisterReq
	err := ctx.ShouldBind(&reqBody)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	err, statusCode := c.serv.Register(reqBody)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("USER_CREATED", true, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *auth) Me(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("AUTHORIZED", true, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *auth) Profile(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	userDto, err, statusCode := c.serv.GetProfile(uint(token["id"].(float64)))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("USER_FOUND", userDto, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
