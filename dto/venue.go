package dto

type Venue struct {
	ID       uint   `json:"id"`
	Name     string `json:"name"`
	Owner    string `json:"owner"`
	Capacity uint32 `json:"capacity"`
}

type VenueReq struct {
	Name     string `json:"name"`
	Owner    string `json:"owner"`
	Capacity uint32 `json:"capacity"`
}
