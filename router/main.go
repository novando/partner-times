package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/repository"
	"gitlab.com/arutek/backend/partner-times/service"
	"gorm.io/gorm"
)

func MainRoute(r *gin.Engine, db *gorm.DB) {
	// Init repo
	repoUser := repository.NewUserRepository(db)
	repoCompany := repository.NewCompanyRepository(db)
	repoVenue := repository.NewVenueRepository(db)

	// Init service
	servUser := service.NewUserService(repoUser)
	servCompany := service.NewCompanyService(repoCompany)
	servVenue := service.NewVenueService(repoVenue)
	// Send service to each route

	UserRoute(r, servUser)
	CompanyRoute(r, servCompany)
	VenueRoute(r, servVenue)
}
