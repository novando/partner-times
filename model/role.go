package model

import "gorm.io/gorm"

type Role struct {
	gorm.Model
	CreatedBy string
	UpdatedBy string
	Name      string `gorm:"not null;unique" binding:"required"`
}
