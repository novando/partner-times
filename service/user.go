package service

import (
	"fmt"
	"net/http"

	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
	"golang.org/x/crypto/bcrypt"
)

type UserService interface {
	Register(ReqBody dto.RegisterReq) (err error, statusCode int)
	Login(email string, password string) (token string, err error, statusCode int)
	GetProfile(userId uint) (userDto dto.UserRes, err error, statusCode int)
}

type user struct {
	repoUser repository.UserRepository
}

func NewUserService(repoUser repository.UserRepository) UserService {
	return &user{repoUser: repoUser}
}

func (s *user) Register(ReqBody dto.RegisterReq) (err error, statusCode int) {
	if ReqBody.Password != ReqBody.ConfPass {
		err = fmt.Errorf("PASSWORD_MISSMATCH")
		statusCode = http.StatusBadRequest
		return
	}
	hashedPassword, err := bcrypt.GenerateFromPassword([]byte(ReqBody.Password), 10)
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	payload := &model.User{
		Name:     ReqBody.Name,
		Email:    ReqBody.Email,
		Password: string(hashedPassword),
		RoleID:   ReqBody.RoleID,
	}
	err = s.repoUser.Save(payload)
	return
}

func (s *user) Login(email string, password string) (token string, err error, statusCode int) {
	userData, err := s.repoUser.GetByEmail(email)
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	err = bcrypt.CompareHashAndPassword([]byte(userData.Password), []byte(password))
	if err != nil {
		statusCode = http.StatusBadRequest
		err = fmt.Errorf("USER_NOT_FOUND")
		return
	}
	payload := map[string]interface{}{
		"id":        userData.ID,
		"name":      userData.Name,
		"email":     userData.Email,
		"roleId":    userData.RoleID,
		"companyId": userData.CompanyID,
	}
	token, statusCode, err = arutek.SignAccessJwt(payload)
	return
}

func (s *user) GetProfile(userId uint) (userDto dto.UserRes, err error, statusCode int) {
	userData, err := s.repoUser.GetById(userId)
	if err != nil {
		statusCode = http.StatusInternalServerError
		return
	}
	userDto = dto.UserRes{
		Name:        userData.Name,
		Email:       userData.Email,
		RoleName:    userData.Role.Name,
		CompanyName: userData.Company.Name,
		CompanyLogo: userData.Company.Logo,
	}
	return
}
