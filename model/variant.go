package model

import (
	"gorm.io/gorm"
)

type Variant struct {
	gorm.Model
	CreatedBy   string
	UpdatedBy   string
	Name        string
	Price       float64 `gorm:"not null" binding:"required"`
	MaxCapacity float32 `gorm:"not null" binding:"required"`
	Duration    uint    `gorm:"not null" binding:"required"`
	Days        []uint8 `binding:"required"`
	PackageID   uint
	Package     Package `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
