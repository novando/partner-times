-- roles
CREATE TABLE IF NOT EXISTS roles(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) UNIQUE NOT NULL,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP NOT NULL
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE roles ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE roles ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE roles ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS roles_deleted_at ON roles(deleted_at);
UPDATE roles SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE roles SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE roles DROP COLUMN updated_at;
ALTER TABLE roles DROP COLUMN created_at;
ALTER TABLE roles RENAME COLUMN new_created_at TO created_at;
ALTER TABLE roles RENAME COLUMN new_updated_at TO updated_at;

-- companies
CREATE TABLE IF NOT EXISTS companies(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  logo VARCHAR(255) NOT NULL,
  owner VARCHAR(255) NOT NULL,
  secretary VARCHAR(255) NOT NULL,
  finance VARCHAR(255) NOT NULL,
  province VARCHAR(255) NOT NULL,
  city VARCHAR(255) NOT NULL,
  address VARCHAR(255) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP NOT NULL
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN logo TYPE TEXT;
ALTER TABLE variants ALTER COLUMN owner TYPE TEXT;
ALTER TABLE variants ALTER COLUMN secretary TYPE TEXT;
ALTER TABLE variants ALTER COLUMN finance TYPE TEXT;
ALTER TABLE variants ALTER COLUMN province TYPE TEXT;
ALTER TABLE variants ALTER COLUMN city TYPE TEXT;
ALTER TABLE variants ALTER COLUMN address TYPE TEXT;
ALTER TABLE variants ALTER COLUMN phone TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE companies ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE companies ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE companies ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS companies_deleted_at ON companies(deleted_at);
UPDATE companies SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE companies SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE companies DROP COLUMN updated_at;
ALTER TABLE companies DROP COLUMN created_at;
ALTER TABLE companies RENAME COLUMN new_created_at TO created_at;
ALTER TABLE companies RENAME COLUMN new_updated_at TO updated_at;

-- users
CREATE TABLE IF NOT EXISTS users(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  email VARCHAR(255) UNIQUE NOT NULL,
  password VARCHAR(255) NOT NULL,
  company_id BIGINT,
  role_id BIGINT DEFAULT 1,
  email_verified_at TIMESTAMP,
  remember_token VARCHAR(255),
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP NOT NULL
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN email TYPE TEXT;
ALTER TABLE variants ALTER COLUMN password TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE users ADD CONSTRAINT users_company_id_fk FOREIGN KEY (company_id) REFERENCES companies(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE users ADD CONSTRAINT users_role_id_fk FOREIGN KEY (role_id) REFERENCES roles(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE users ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE users ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE users ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS users_deleted_at ON users(deleted_at);
UPDATE users SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE users SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE users DROP COLUMN updated_at;
ALTER TABLE users DROP COLUMN created_at;
ALTER TABLE users RENAME COLUMN new_created_at TO created_at;
ALTER TABLE users RENAME COLUMN new_updated_at TO updated_at;
ALTER TABLE users DROP COLUMN email_verified_at;
ALTER TABLE users DROP COLUMN remember_token;

-- venues
CREATE TABLE IF NOT EXISTS venues(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  owner VARCHAR(255) NOT NULL,
  deleted BOOLEAN DEFAULT false,
  capacity INTEGER NOT NULL,
  company_id BIGINT,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN owner TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE venues ADD CONSTRAINT venues_company_id_fk FOREIGN KEY (company_id) REFERENCES companies(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE venues ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE venues ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE venues ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS venues_deleted_at ON venues(deleted_at);
UPDATE venues SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE venues SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
UPDATE venues SET deleted_at = CASE WHEN deleted = true THEN '1970-01-01 07:00:00+07'::TIMESTAMP WITH TIME ZONE ELSE NULL END;
ALTER TABLE venues DROP COLUMN updated_at;
ALTER TABLE venues DROP COLUMN created_at;
ALTER TABLE venues DROP COLUMN deleted;
ALTER TABLE venues RENAME COLUMN new_created_at TO created_at;
ALTER TABLE venues RENAME COLUMN new_updated_at TO updated_at;

-- packages
CREATE TABLE IF NOT EXISTS packages(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  description VARCHAR(255),
  company_id BIGINT NOT NULL,
  deleted BOOLEAN DEFAULT false,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN description TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE packages ALTER COLUMN company_id DROP NOT NULL;
ALTER TABLE packages ADD CONSTRAINT packages_company_id_fk FOREIGN KEY (company_id) REFERENCES companies(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE packages ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE packages ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE packages ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS packages_deleted_at ON packages(deleted_at);
UPDATE packages SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE packages SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
UPDATE packages SET deleted_at = CASE WHEN deleted = true THEN '1970-01-01 07:00:00+07'::TIMESTAMP WITH TIME ZONE ELSE NULL END;
ALTER TABLE packages DROP COLUMN updated_at;
ALTER TABLE packages DROP COLUMN created_at;
ALTER TABLE packages DROP COLUMN deleted;
ALTER TABLE packages RENAME COLUMN new_created_at TO created_at;
ALTER TABLE packages RENAME COLUMN new_updated_at TO updated_at;

-- ticket_statuses
CREATE TABLE IF NOT EXISTS ticket_statuses(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE ticket_statuses ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE ticket_statuses ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE ticket_statuses ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS ticket_statuses_deleted_at ON ticket_statuses(deleted_at);
UPDATE ticket_statuses SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE ticket_statuses SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE ticket_statuses DROP COLUMN updated_at;
ALTER TABLE ticket_statuses DROP COLUMN created_at;
ALTER TABLE ticket_statuses RENAME COLUMN new_created_at TO created_at;
ALTER TABLE ticket_statuses RENAME COLUMN new_updated_at TO updated_at;

-- variants
CREATE TABLE IF NOT EXISTS variants(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255),
  price NUMERIC(9,2) NOT NULL,
  max_capacity NUMERIC(3,1) NOT NULL,
  duration INT NOT NULL,
  days JSON DEFAULT '[0,1,2,3,4,5,6]'::JSON,
  deleted BOOLEAN DEFAULT false,
  package_id BIGINT,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ADD CONSTRAINT variants_package_id_fk FOREIGN KEY (package_id) REFERENCES packages(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN price TYPE NUMERIC;
ALTER TABLE variants ALTER COLUMN max_capacity TYPE NUMERIC;
ALTER TABLE variants ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE variants ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE variants ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS variants_deleted_at ON variants(deleted_at);
UPDATE variants SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE variants SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
UPDATE variants SET deleted_at = CASE WHEN deleted = true THEN '1970-01-01 07:00:00+07'::TIMESTAMP WITH TIME ZONE ELSE NULL END;
ALTER TABLE variants DROP COLUMN updated_at;
ALTER TABLE variants DROP COLUMN created_at;
ALTER TABLE variants DROP COLUMN deleted;
ALTER TABLE variants RENAME COLUMN new_created_at TO created_at;
ALTER TABLE variants RENAME COLUMN new_updated_at TO updated_at;

-- tickets
CREATE TABLE IF NOT EXISTS tickets(
  id BIGSERIAL PRIMARY KEY,
  id_ticket VARCHAR(255) NOT NULL,
  phone VARCHAR(255) NOT NULL,
  pic VARCHAR(255),
  price NUMERIC(9,2),
  venue_id BIGINT,
  variant_id BIGINT NOT NULL,
  ticket_status_id BIGINT DEFAULT 1,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ALTER COLUMN id_ticket TYPE TEXT;
ALTER TABLE variants ALTER COLUMN phone TYPE TEXT;
ALTER TABLE variants ALTER COLUMN pic TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
ALTER TABLE tickets ALTER COLUMN variant_id DROP NOT NULL;
ALTER TABLE tickets ALTER COLUMN phone DROP NOT NULL;
ALTER TABLE tickets ALTER COLUMN price TYPE NUMERIC;
ALTER TABLE tickets ADD CONSTRAINT tickets_ticket_status_id_fk FOREIGN KEY (ticket_status_id) REFERENCES ticket_statuses(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE tickets ADD CONSTRAINT tickets_variant_id_fk FOREIGN KEY (variant_id) REFERENCES variants(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE tickets ADD CONSTRAINT tickets_venue_id_fk FOREIGN KEY (venue_id) REFERENCES venues(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE tickets ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE tickets ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE tickets ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS tickets_deleted_at ON tickets(deleted_at);
UPDATE tickets SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE tickets SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE tickets DROP COLUMN updated_at;
ALTER TABLE tickets DROP COLUMN created_at;
ALTER TABLE tickets RENAME COLUMN new_created_at TO created_at;
ALTER TABLE tickets RENAME COLUMN new_updated_at TO updated_at;

-- paxes
CREATE TABLE IF NOT EXISTS paxes(
  id BIGSERIAL PRIMARY KEY,
  name VARCHAR(255) NOT NULL,
  sex VARCHAR(255) NOT NULL,
  age SMALLINT NOT NULL,
  province VARCHAR(255) NOT NULL,
  city VARCHAR(255) NOT NULL,
  address VARCHAR(255),
  registrant BOOLEAN DEFAULT false,
  ticket_id BIGINT,
  created_by VARCHAR(255),
  updated_by VARCHAR(255),
  created_at TIMESTAMP,
  updated_at TIMESTAMP
);
ALTER TABLE variants ALTER COLUMN name TYPE TEXT;
ALTER TABLE variants ALTER COLUMN sex TYPE TEXT;
ALTER TABLE variants ALTER COLUMN province TYPE TEXT;
ALTER TABLE variants ALTER COLUMN city TYPE TEXT;
ALTER TABLE variants ALTER COLUMN address TYPE TEXT;
ALTER TABLE variants ALTER COLUMN created_by TYPE TEXT;
ALTER TABLE variants ALTER COLUMN updated_by TYPE TEXT;
DELETE FROM paxes WHERE ticket_id NOT IN (SELECT id FROM tickets);
ALTER TABLE paxes ADD CONSTRAINT paxes_ticket_id_fk FOREIGN KEY (ticket_id) REFERENCES tickets(id) ON UPDATE CASCADE ON DELETE SET NULL;
ALTER TABLE paxes ADD COLUMN new_created_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE paxes ADD COLUMN new_updated_at TIMESTAMP WITH TIME ZONE;
ALTER TABLE paxes ADD COLUMN deleted_at TIMESTAMP WITH TIME ZONE;
CREATE INDEX IF NOT EXISTS paxes_deleted_at ON paxes(deleted_at);
UPDATE paxes SET new_created_at = created_at::TIMESTAMP WITH TIME ZONE;
UPDATE paxes SET new_updated_at = updated_at::TIMESTAMP WITH TIME ZONE;
ALTER TABLE paxes DROP COLUMN updated_at;
ALTER TABLE paxes DROP COLUMN created_at;
ALTER TABLE paxes RENAME COLUMN new_created_at TO created_at;
ALTER TABLE paxes RENAME COLUMN new_updated_at TO updated_at;

DROP TABLE IF EXISTS manifests;
DROP TABLE IF EXISTS migrations;
DROP TABLE IF EXISTS failed_jobs;
DROP TABLE IF EXISTS password_resets;
DROP TABLE IF EXISTS personal_access_tokens;