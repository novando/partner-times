package service

import (
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type TicketService interface {
	Get(companyId uint, page int, size int) (ticketDto []dto.TicketRes, count int64)
	GetUnfinished(companyId uint, page int, size int) (ticketDto []dto.TicketRes, count int64)
	GetDetail(ticketId uint) (ticketDto dto.TicketDetailRes, err error)
	Create(ticketDto *dto.TicketReq) error
	Update(ticketId uint, ticketDto *dto.TicketEditReq) error
}

type ticket struct {
	repoPack repository.PackageRepository
	repoVari repository.VariantRepository
	repoTick repository.TicketRepository
	repoPax  repository.PaxRepository
}

func NewTicketService(
	rp repository.PackageRepository,
	rv repository.VariantRepository,
	rt repository.TicketRepository,
	rpx repository.PaxRepository,
) TicketService {
	return &ticket{
		repoPack: rp,
		repoVari: rv,
		repoTick: rt,
		repoPax:  rpx,
	}
}

func (s *ticket) Get(companyId uint, page int, size int) (ticketDto []dto.TicketRes, count int64) {
	ticketData := s.repoTick.GetByCompany(companyId, page, size)
	count = s.repoTick.CountByCompany(companyId)
	for _, val := range ticketData {
		ticketDto = append(ticketDto, dto.TicketRes{
			ID:             val.ID,
			IdTicket:       val.IdTicket,
			Phone:          val.Phone,
			Pic:            val.Pic,
			Price:          val.Price,
			VenueID:        val.VenueID,
			VariantID:      val.VariantID,
			VariantName:    val.Variant.Name,
			PackageName:    val.Variant.Package.Name,
			TicketStatusID: val.TicketStatusID,
		})
	}
	return
}

func (s *ticket) GetUnfinished(companyId uint, page int, size int) (ticketDto []dto.TicketRes, count int64) {
	ticketData := s.repoTick.GetByCompanyUnfinished(companyId, page, size)
	count = s.repoTick.CountByCompanyUnfinished(companyId)
	for _, val := range ticketData {
		ticketDto = append(ticketDto, dto.TicketRes{
			ID:             val.ID,
			IdTicket:       val.IdTicket,
			Phone:          val.Phone,
			Pic:            val.Pic,
			Price:          val.Price,
			VenueID:        val.VenueID,
			VariantID:      val.VariantID,
			VariantName:    val.Variant.Name,
			PackageName:    val.Variant.Package.Name,
			TicketStatusID: val.TicketStatusID,
		})
	}
	return
}

func (s *ticket) GetDetail(ticketId uint) (ticketDto dto.TicketDetailRes, err error) {
	ticketData, err := s.repoTick.GetById(ticketId)
	if err != nil {
		return
	}
	var paxDto []dto.PaxRes
	paxData := s.repoPax.GetByTicketId(ticketId)
	for _, val := range paxData {
		paxDto = append(paxDto, dto.PaxRes{
			Name:       val.Name,
			Sex:        val.Sex,
			Age:        val.Age,
			Province:   val.Province,
			City:       val.City,
			Address:    val.Address,
			Registrant: val.Registrant,
			TicketID:   val.TicketID,
		})
	}
	ticketDto = dto.TicketDetailRes{
		TicketRes: dto.TicketRes{
			ID:             ticketData.ID,
			IdTicket:       ticketData.IdTicket,
			Phone:          ticketData.Phone,
			Pic:            ticketData.Pic,
			Price:          ticketData.Price,
			VenueID:        ticketData.VenueID,
			VariantID:      ticketData.VariantID,
			VariantName:    ticketData.Variant.Name,
			PackageName:    ticketData.Variant.Package.Name,
			TicketStatusID: ticketData.TicketStatusID,
		},
		Pax: paxDto,
	}
	return
}

func (s *ticket) Create(ticketDto *dto.TicketReq) error {
	return s.repoTick.Save(&model.Ticket{
		IdTicket:       ticketDto.IdTicket,
		VariantID:      ticketDto.VariantID,
		TicketStatusID: ticketDto.TicketStatusID,
	})
}

func (s *ticket) Update(ticketId uint, ticketDto *dto.TicketEditReq) error {
	ticketData, err := s.repoTick.GetById(ticketId)
	if err != nil {
		return err
	}
	ticketData.Pic = ticketDto.Pic
	ticketData.Price = ticketDto.Price
	ticketData.VenueID = ticketDto.VenueID
	ticketData.TicketStatusID = ticketDto.TicketStatusID
	return s.repoTick.Save(&ticketData)
}
