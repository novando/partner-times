package main

import (
	"fmt"
	"os"
	"path/filepath"
	"runtime"
	"time"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"github.com/golang-migrate/migrate/v4"
	_ "github.com/golang-migrate/migrate/v4/database/postgres"
	_ "github.com/golang-migrate/migrate/v4/source/file"
	"github.com/joho/godotenv"
	"gitlab.com/arutek/backend/partner-times/router"
)

func main() {
	// THIS BLOCK IS MANDATORY
	_, filename, _, ok := runtime.Caller(0)
	if !ok {
		arutek.Logger("unable to get the current filename")
	}
	dirPath := filepath.Dir(filename)
	envPath := fmt.Sprint(dirPath, "/.env")
	if err := godotenv.Load(envPath); err != nil {
		arutek.LoggerErr("Error loading .env file")
		panic(err)
	}
	location, err := time.LoadLocation("Asia/Jakarta")
	if err != nil {
		arutek.LoggerErr("Error loading locale time")
		panic(err)
	}
	time.Local = location

	// Core packages initialization
	port := os.Getenv("APP_PORT")
	r := gin.Default()
	r.Use(cors.Default())

	// DB migrate
	dbHost := os.Getenv("GORMDB_HOST")
	dbUser := os.Getenv("GORMDB_USER")
	dbPass := os.Getenv("GORMDB_PASS")
	dbName := os.Getenv("GORMDB_NAME")
	dbPort := os.Getenv("GORMDB_PORT")
	dbSsl := os.Getenv("GORMDB_SSL")
	dbTz := os.Getenv("GORMDB_TZ")
	dbConn := fmt.Sprintf("postgres://%v:%v@%v:%v/%v?sslmode=%v", dbUser, dbPass, dbHost, dbPort, dbName, dbSsl)
	m, err := migrate.New(
		fmt.Sprint("file://"+dirPath+"/db"),
		dbConn)
	if err != nil {
		arutek.LoggerErr(err.Error())
		panic(err)
	}
	if err := m.Up(); err != nil {
		arutek.LoggerErr(err.Error())
		panic(err)
	}

	// DB connect
	db := arutek.GormInit(dbHost, dbUser, dbPass, dbName, dbPort, dbSsl, dbTz)
	sqlDb, err := db.DB()
	if err != nil {
		arutek.LoggerErr(err.Error())
		panic(err)
	}
	sqlDb.SetMaxIdleConns(3)
	sqlDb.SetMaxOpenConns(30)
	sqlDb.SetConnMaxLifetime(30 * time.Minute)
	arutek.Logger("Connected with DB for partner-lines")

	// Start app
	router.MainRoute(r, db)
	r.Run(fmt.Sprint(":", port))
}
