package service

import (
	"errors"

	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type VenueService interface {
	GetAll(page int, size int) (venueDto []dto.Venue, total int64)
	GetByCompany(companyId uint, page int, size int) (venueDto []dto.Venue, total int64)
	GetDetail(id uint) dto.Venue
	Create(venueReq dto.VenueReq, companyId uint) error
	Update(id uint, venueReq dto.VenueReq) error
	Delete(id uint) error
}

type venue struct {
	repo repository.VenueRepository
}

func NewVenueService(rv repository.VenueRepository) VenueService {
	return &venue{repo: rv}
}

func (s *venue) GetAll(page int, size int) (venueDto []dto.Venue, total int64) {
	venueData := s.repo.Get(page, size)
	total = s.repo.Count(-1)
	for _, val := range venueData {
		venueDto = append(venueDto, dto.Venue{
			ID:       val.ID,
			Name:     val.Name,
			Owner:    val.Owner,
			Capacity: val.Capacity,
		})
	}
	return
}

func (s *venue) GetByCompany(companyId uint, page int, size int) (venueDto []dto.Venue, total int64) {
	venueData := s.repo.GetByCompany(companyId, page, size)
	total = s.repo.Count(-1)
	for _, val := range venueData {
		venueDto = append(venueDto, dto.Venue{
			ID:       val.ID,
			Name:     val.Name,
			Owner:    val.Owner,
			Capacity: val.Capacity,
		})
	}
	return
}

func (s *venue) GetDetail(id uint) dto.Venue {
	venueData := s.repo.GetOne(id)
	return dto.Venue{
		ID:       venueData.ID,
		Name:     venueData.Name,
		Owner:    venueData.Owner,
		Capacity: venueData.Capacity,
	}
}

func (s *venue) Create(venueReq dto.VenueReq, companyId uint) error {
	venueData := model.Venue{
		Name:      venueReq.Name,
		Owner:     venueReq.Owner,
		Capacity:  venueReq.Capacity,
		CompanyID: companyId,
	}
	return s.repo.Save(&venueData)
}

func (s *venue) Update(id uint, venueReq dto.VenueReq) error {
	venueData := s.repo.GetOne(id)
	if venueData.ID != id {
		return errors.New("VENUE_NOT_FOUND")
	}
	venueData.Name = venueReq.Name
	venueData.Owner = venueReq.Owner
	venueData.Capacity = venueReq.Capacity
	return s.repo.Save(&venueData)
}

func (s *venue) Delete(id uint) error {
	return s.repo.Delete(id)
}
