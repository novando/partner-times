package controller

import (
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type role struct {
	serv service.RoleService
}

func NewRoleController(sr service.RoleService) *role {
	return &role{serv: sr}
}

func (c *role) Get(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "ROLES_FOUND"
	roleData, count := c.serv.Get(pagingPage, pagingSize)
	if count == 0 {
		msgVal = "ROLE_NOT_FOUND"
	}
	res := arutek.Response(msgVal, roleData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *role) GetDetail(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	roleId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	roleData, err := c.serv.GetById(uint(roleId))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("ROLE_FOUND", roleData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *role) Create(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var roleData dto.RoleReq
	if err = ctx.ShouldBind(&roleData); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Create(&roleData); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("ROLE_CREATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *role) Update(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	roleId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var roleData dto.RoleReq
	if err = ctx.ShouldBind(&roleData); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Update(uint(roleId), &roleData); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("ROLE_UPDATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *role) Delete(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	roleId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Delete(uint(roleId)); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("ROLE_DELETED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
