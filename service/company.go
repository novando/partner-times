package service

import (
	"net/http"

	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type CompanyService interface {
	Get() (companyDto []dto.CompanyRes)
	GetDetail(companyId uint) (companyDto dto.CompanyRes, err error, statusCode int)
	Delete(companyId uint) (err error, statusCode int)
	Create(companyDto *dto.CompanyReq) (err error, statusCode int)
	Update(companyDto *dto.CompanyEditReq, companyId uint) (err error, statusCode int)
}

type company struct {
	repo repository.CompanyRepository
}

func NewCompanyService(c repository.CompanyRepository) CompanyService {
	return &company{repo: c}
}

func (s *company) Get() (companyDto []dto.CompanyRes) {
	companyData := s.repo.Get()
	for _, val := range companyData {
		companyDto = append(companyDto, dto.CompanyRes{
			ID:        val.ID,
			Name:      val.Name,
			Logo:      val.Logo,
			Owner:     val.Owner,
			Secretary: val.Secretary,
			Finance:   val.Finance,
			Province:  val.Province,
			City:      val.City,
			Address:   val.Address,
			Phone:     val.Phone,
		})
	}
	return
}

func (s *company) GetDetail(companyId uint) (companyDto dto.CompanyRes, err error, statusCode int) {
	companyData, err := s.repo.GetById(companyId)
	if err != nil {
		return
	}
	companyDto = dto.CompanyRes{
		ID:        companyData.ID,
		Name:      companyData.Name,
		Logo:      companyData.Logo,
		Owner:     companyData.Owner,
		Secretary: companyData.Secretary,
		Finance:   companyData.Finance,
		Province:  companyData.Province,
		City:      companyData.City,
		Address:   companyData.Address,
		Phone:     companyData.Phone,
	}
	return
}

func (s *company) Delete(companyId uint) (err error, statusCode int) {
	if err = s.repo.Delete(companyId); err != nil {
		statusCode = http.StatusBadRequest
	}
	return
}

func (s *company) Create(companyDto *dto.CompanyReq) (err error, statusCode int) {
	if err = s.repo.Save(&model.Company{
		Name:     companyDto.Name,
		Owner:    companyDto.Owner,
		Province: companyDto.Province,
		City:     companyDto.City,
		Phone:    companyDto.Phone,
	}); err != nil {
		statusCode = http.StatusBadRequest
	}
	return
}

func (s *company) Update(companyDto *dto.CompanyEditReq, companyId uint) (err error, statusCode int) {
	companyData, err := s.repo.GetById(companyId)
	if err != nil {
		statusCode = http.StatusBadRequest
		return
	}
	companyData.Name = companyDto.Name
	companyData.Secretary = companyDto.Secretary
	companyData.Finance = companyDto.Finance
	companyData.Owner = companyDto.Owner
	companyData.Province = companyDto.Province
	companyData.City = companyDto.City
	companyData.Phone = companyDto.Phone
	if err = s.repo.Save(&companyData); err != nil {
		statusCode = http.StatusBadRequest
	}
	return
}
