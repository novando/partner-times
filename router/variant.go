package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func VariantRoute(r *gin.Engine, serv service.VariantService) {
	cont := controller.NewVariantController(serv)
	route := r.Group("/variant")

	route.GET("/packet/:packetId", cont.GetByPackages)
	route.DELETE("/:id", cont.Delete)
	route.PUT("/:id", cont.Update)
	route.GET("/:id", cont.GetDetail)
	route.POST("", cont.Create)
	route.GET("", cont.GetAll)
}
