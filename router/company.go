package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func CompanyRoute(r *gin.Engine, serv service.CompanyService) {
	CompanyController := controller.NewCompanyController(serv)
	route := r.Group("/company")

	route.DELETE("/:id", CompanyController.DeleteCompany)
	route.PUT("/:id", CompanyController.UpdateCompany)
	route.GET("/:id", CompanyController.FindByIdCompany)
	route.GET("", CompanyController.FindAllCompanies)
	route.POST("", CompanyController.CreateCompany)
}
