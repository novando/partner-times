package dto

type RoleRes struct {
	ID   uint   `json:"id"`
	Name string `json:"name"`
}
type RoleReq struct {
	Name string `json:"name"`
}
