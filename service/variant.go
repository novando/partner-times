package service

import (
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type VariantService interface {
	Get(companyId uint, page int, size int) (variantDto []dto.VariantRes, count int64)
	GetByPackage(packageId uint, page int, size int) (variantDto []dto.VariantRes, count int64)
	GetDetail(variantId uint) dto.VariantRes
	Delete(variantId uint) error
	Update(variantId uint, variant *dto.VariantEditReq) error
	Create(variant *dto.VariantReq) error
}

type variant struct {
	repoVar repository.VariantRepository
	repoPac repository.PackageRepository
}

func NewVariantService(
	rVar repository.VariantRepository,
	rPac repository.PackageRepository,
) VariantService {
	return &variant{
		repoVar: rVar,
		repoPac: rPac,
	}
}

func (s *variant) Get(companyId uint, page int, size int) (variantDto []dto.VariantRes, count int64) {
	variantData := s.repoVar.GetByCompany(companyId, page, size)
	count = s.repoVar.CountByCompany(companyId)
	for _, val := range variantData {
		variantDto = append(variantDto, dto.VariantRes{
			ID:          val.ID,
			Name:        val.Package.Name + " - " + val.Name,
			MaxCapacity: uint16(val.MaxCapacity),
			Duration:    uint16(val.Duration),
			Price:       val.Price,
			Days:        val.Days,
		})
	}
	return
}

func (s *variant) GetByPackage(packageId uint, page int, size int) (variantDto []dto.VariantRes, count int64) {
	variantData := s.repoVar.GetByPackages([]uint{packageId}, page, size)
	count = s.repoVar.CountByPackages([]uint{packageId})
	for _, val := range variantData {
		variantDto = append(variantDto, dto.VariantRes{
			ID:          val.ID,
			Name:        val.Package.Name + " - " + val.Name,
			MaxCapacity: uint16(val.MaxCapacity),
			Duration:    uint16(val.Duration),
			Price:       val.Price,
			Days:        val.Days,
		})
	}
	return
}

func (s *variant) GetDetail(variantId uint) dto.VariantRes {
	variantData := s.repoVar.GetById(variantId)
	return dto.VariantRes{
		ID:          variantData.ID,
		Name:        variantData.Package.Name + " - " + variantData.Name,
		MaxCapacity: uint16(variantData.MaxCapacity),
		Duration:    uint16(variantData.Duration),
		Price:       variantData.Price,
		Days:        variantData.Days,
	}
}

func (s *variant) Delete(variantId uint) error {
	return s.repoVar.Delete(variantId)
}

func (s *variant) Update(variantId uint, variant *dto.VariantEditReq) error {
	variantData := s.repoVar.GetById(variantId)
	variantData.Name = variant.Name
	variantData.Days = variant.Days
	return s.repoVar.Save(&variantData)
}

func (s *variant) Create(variant *dto.VariantReq) error {
	return s.repoVar.Save(&model.Variant{
		Name:        variant.Name,
		MaxCapacity: float32(variant.MaxCapacity),
		Duration:    uint(variant.Duration),
		Price:       variant.Price,
		PackageID:   variant.PackageID,
	})
}
