package dto

type TicketRes struct {
	ID             uint    `json:"id"`
	IdTicket       string  `json:"idTicket"`
	Phone          string  `json:"phone"`
	Pic            string  `json:"pic"`
	Price          float64 `json:"price"`
	VenueID        uint    `json:"venueId"`
	VariantID      uint    `json:"variantId"`
	VariantName    string  `json:"variantName"`
	PackageName    string  `json:"packageName"`
	TicketStatusID uint    `json:"ticketStatusId"`
}

type PaxRes struct {
	Name       string `json:"name"`
	Sex        string `json:"sex"`
	Age        uint8  `json:"age"`
	Province   string `json:"province"`
	City       string `json:"city"`
	Address    string `json:"address"`
	Registrant bool   `json:"registrant"`
	TicketID   uint   `json:"ticketId"`
}

type TicketDetailRes struct {
	TicketRes
	Pax []PaxRes `json:"pax"`
}

type TicketReq struct {
	IdTicket       string `json:"idTicket"`
	VariantID      uint   `json:"variantId"`
	TicketStatusID uint   `json:"ticketStatusId"`
}

type TicketEditReq struct {
	IdTicket       string  `json:"idTicket"`
	Price          float64 `json:"price"`
	Pic            string  `json:"pic"`
	VenueID        uint    `json:"venueId"`
	TicketStatusID uint    `json:"ticketStatusId"`
}
