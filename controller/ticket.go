package controller

import (
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type ticket struct {
	serv service.TicketService
}

func NewTicketController(ts service.TicketService) *ticket {
	return &ticket{serv: ts}
}

func (c *ticket) Get(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "TICKETS_FOUND"
	companyId := uint(token["companyId"].(float64))
	ticketData, count := c.serv.Get(companyId, pagingPage, pagingSize)
	if count == 0 {
		msgVal = "TICKET_NOT_FOUND"
	}
	res := arutek.Response(msgVal, ticketData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *ticket) GetUnfinished(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "TICKETS_FOUND"
	companyId := uint(token["companyId"].(float64))
	ticketData, count := c.serv.GetUnfinished(companyId, pagingPage, pagingSize)
	if count == 0 {
		msgVal = "TICKET_NOT_FOUND"
	}
	res := arutek.Response(msgVal, ticketData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *ticket) GetDetail(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	ticketId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	ticketData, err := c.serv.GetDetail(uint(ticketId))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("TICKET_FOUND", ticketData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *ticket) Create(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "TICKETS_FOUND"
	companyId := uint(token["companyId"].(float64))
	ticketData, count := c.serv.GetUnfinished(companyId, pagingPage, pagingSize)
	if count == 0 {
		msgVal = "TICKET_NOT_FOUND"
	}
	res := arutek.Response(msgVal, ticketData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *ticket) Update(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	ticketId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var ticketData dto.TicketEditReq
	if err = ctx.ShouldBind(&ticketData); err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Update(uint(ticketId), &ticketData); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("TICKET_UPDATED", ticketData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
