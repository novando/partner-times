package repository

import (
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type PaxRepository interface {
	Create(paxData model.Pax) error
	GetByTicketId(ticketId uint) []model.Pax
	GetByTicketCode(ticketCode string) []model.Pax
}

type pax struct {
	db *gorm.DB
}

func NewPaxRepository(db *gorm.DB) PaxRepository {
	return &pax{db: db}
}

func (r *pax) Create(paxData model.Pax) error {
	return r.db.Create(&paxData).Error
}

func (r *pax) GetByTicketId(ticketId uint) []model.Pax {
	var paxData []model.Pax
	r.db.Preload("Ticket").Where("ticket_id = ?", ticketId).Find(&paxData)
	return paxData
}

func (r *pax) GetByTicketCode(ticketCode string) []model.Pax {
	var paxData []model.Pax
	r.db.Preload("Ticket").Where("ticket_id = (SELECT (id) FROM tickets WHERE id_ticket = ?)", ticketCode).Find(&paxData)
	return paxData
}
