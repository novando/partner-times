package repository

import (
	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type VenueRepository interface {
	Get(page int, size int) []model.Venue
	GetByCompany(companyId uint, page int, size int) []model.Venue
	GetOne(id uint) model.Venue
	Save(venueData *model.Venue) error
	Delete(id uint) error
	Count(companyId int64) int64
}

type venue struct {
	db *gorm.DB
}

func NewVenueRepository(db *gorm.DB) VenueRepository {
	return &venue{db: db}
}

func (r *venue) Get(page int, size int) []model.Venue {
	var venueData []model.Venue
	r.db.Preload("Company").Scopes(arutek.GormPaginate(page, size)).Order("name ASC").Find(&venueData)
	return venueData
}

func (r *venue) GetByCompany(companyId uint, page int, size int) []model.Venue {
	var venueData []model.Venue
	r.db.Preload("Company").Where("company_id = ?", companyId).Scopes(arutek.GormPaginate(page, size)).Order("name ASC").Find(&venueData)
	return venueData
}

func (r *venue) GetOne(id uint) model.Venue {
	var venueData model.Venue
	r.db.Preload("Company").First(&venueData, id)
	return venueData
}

func (r *venue) Save(venueData *model.Venue) error {
	return r.db.Save(&venueData).Error
}

func (r *venue) Delete(id uint) error {
	return r.db.Delete(&model.Venue{}, id).Error
}

func (r *venue) Count(companyId int64) int64 {
	var total int64
	query := r.db.Table("venues")
	if companyId < 0 {
		query.Where("company_id = ?", companyId)
	}
	query.Count(&total)
	return total
}
