package model

import "gorm.io/gorm"

type TicketStatus struct {
	gorm.Model
	CreatedBy string
	UpdatedBy string
	Name      string `gorm:"not null" binding:"required"`
}
