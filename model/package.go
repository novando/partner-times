package model

import "gorm.io/gorm"

type Package struct {
	gorm.Model
	CreatedBy   string
	UpdatedBy   string
	Name        string `gorm:"not null" binding:"required"`
	Description string
	CompanyID   uint    `gorm:"not null;default:1" binding:"required"`
	Company     Company `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
