package model

import "gorm.io/gorm"

type Pax struct {
	gorm.Model
	CreatedBy  string
	UpdatedBy  string
	Name       string `gorm:"not null" binding:"required"`
	Sex        string `gorm:"not null" binding:"required; oneof=MALE FEMALE"`
	Registrant bool   `gorm:"not null; default:false"`
	Age        uint8  `gorm:"not null" binding:"required"`
	Province   string `gorm:"not null" binding:"required"`
	City       string `gorm:"not null" binding:"required"`
	Address    string
	TicketID   uint
	Ticket     Ticket `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
