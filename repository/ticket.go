package repository

import (
	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type TicketRepository interface {
	GetByVariants(variantIds []uint, page int, size int) []model.Ticket
	CountByVariants(companyId []uint) int64
	GetByCompany(companyId uint, page int, size int) []model.Ticket
	CountByCompany(companyId uint) int64
	GetByCompanyUnfinished(companyId uint, page int, size int) []model.Ticket
	CountByCompanyUnfinished(companyId uint) int64
	GetByCode(code string) (model.Ticket, error)
	GetById(id uint) (model.Ticket, error)
	Save(ticketData *model.Ticket) error
}

type ticket struct {
	db *gorm.DB
}

func NewTicketRepository(db *gorm.DB) TicketRepository {
	return &ticket{db: db}
}

func (r *ticket) GetByVariants(variantIds []uint, page int, size int) []model.Ticket {
	var ticketData []model.Ticket
	r.db.Preload("TicketStatus").Preload("Venue").Preload("Variant").Scopes(arutek.GormPaginate(page, size)).Where("variant_id IN ?", variantIds).Find(&ticketData)
	return ticketData
}

func (r *ticket) CountByVariants(variantIds []uint) int64 {
	var countData int64
	r.db.Table("tickets").Where("variant_id IN ?", variantIds).Count(&countData)
	return countData
}

func (r *ticket) GetByCompany(companyId uint, page int, size int) []model.Ticket {
	var ticketData []model.Ticket
	r.db.Preload("TicketStatus").Preload("Venue").Preload("Variant").Scopes(arutek.GormPaginate(page, size)).Where("variant_id IN (SELECT (id) FROM variants WHERE company_id = ?)", companyId).Find(&ticketData)
	return ticketData
}

func (r *ticket) CountByCompany(companyId uint) int64 {
	var countData int64
	r.db.Table("tickets").Where("variant_id IN (SELECT (id) FROM variants WHERE company_id = ?)", companyId).Count(&countData)
	return countData
}

func (r *ticket) GetByCompanyUnfinished(companyId uint, page int, size int) []model.Ticket {
	var ticketData []model.Ticket
	r.db.Preload("TicketStatus").Preload("Venue").Preload("Variant").Scopes(arutek.GormPaginate(page, size)).Where("variant_id IN (SELECT (id) FROM variants WHERE company_id = ?) AND ticket_status_id = 1", companyId).Find(&ticketData)
	return ticketData
}

func (r *ticket) CountByCompanyUnfinished(companyId uint) int64 {
	var countData int64
	r.db.Table("tickets").Where("variant_id IN (SELECT (id) FROM variants WHERE company_id = ?) AND ticket_status_id = 1", companyId).Count(&countData)
	return countData
}

func (r *ticket) GetByCode(code string) (model.Ticket, error) {
	var ticketData model.Ticket
	err := r.db.Preload("TicketStatus").Preload("Venue").Preload("Variant").Where("id_ticket = ?", code).First(&ticketData).Error
	return ticketData, err
}

func (r *ticket) GetById(id uint) (model.Ticket, error) {
	var ticketData model.Ticket
	err := r.db.Preload("TicketStatus").Preload("Venue").Preload("Variant").First(&ticketData, id).Error
	return ticketData, err
}

func (r *ticket) Save(ticketData *model.Ticket) error {
	return r.db.Save(&ticketData).Error
}
