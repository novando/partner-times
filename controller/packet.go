package controller

import (
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type packet struct {
	serv service.PacketService
}

func NewPacketController(sp service.PacketService) *packet {
	return &packet{serv: sp}
}

func (c *packet) Get(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "PACKETS_FOUND"
	companyId := uint(token["companyId"].(float64))
	packetData, count := c.serv.Get(companyId, pagingPage, pagingSize)
	if count == 0 {
		msgVal = "PACKET_NOT_FOUND"
	}
	res := arutek.Response(msgVal, packetData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *packet) GetDetail(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	packetId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	packetData, err := c.serv.GetDetail(uint(packetId))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("PACKET_FOUND", packetData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *packet) Create(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var packetData dto.PacketReq
	if err = ctx.ShouldBind(&packetData); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Create(&packetData); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("PACKET_CREATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *packet) Update(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	packetId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var packetData dto.PacketEditReq
	if err = ctx.ShouldBind(&packetData); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Update(&packetData, uint(packetId)); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("PACKET_UPDATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *packet) Delete(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	packetId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Delete(uint(packetId)); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("PACKET_DELETED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
