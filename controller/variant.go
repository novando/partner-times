package controller

import (
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type variant struct {
	serv service.VariantService
}

func NewVariantController(sv service.VariantService) *variant {
	return &variant{serv: sv}
}

func (c *variant) GetAll(ctx *gin.Context) {
	token, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "VARIANTS_FOUND"
	companyId := uint(token["companyId"].(float64))
	variantData, count := c.serv.Get(companyId, pagingPage, pagingSize)
	if count == 0 {
		msgVal = "VARIANT_NOT_FOUND"
	}
	res := arutek.Response(msgVal, variantData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *variant) GetDetail(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	variantId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	variantData := c.serv.GetDetail(uint(variantId))
	res := arutek.Response("VARIANT_FOUND", variantData, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *variant) GetByPackages(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	packageId, err := strconv.Atoi(ctx.Param("packetId"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	pagingPage, err := strconv.Atoi(ctx.Query("page"))
	if err != nil {
		pagingPage = 0
	}
	pagingSize, err := strconv.Atoi(ctx.Query("size"))
	if err != nil {
		pagingSize = 10
	}
	msgVal := "VARIANTS_FOUND"
	variantData, count := c.serv.GetByPackage(uint(packageId), pagingPage, pagingSize)
	if count == 0 {
		msgVal = "VARIANT_NOT_FOUND"
	}
	res := arutek.Response(msgVal, variantData, count)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *variant) Create(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var variantDto dto.VariantReq
	if err = ctx.ShouldBind(&variantDto); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Create(&variantDto); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("VARIANT_CREATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *variant) Update(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	variantId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var variantDto dto.VariantEditReq
	if err = ctx.ShouldBind(&variantDto); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err = c.serv.Update(uint(variantId), &variantDto); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("VARIANT_UPDATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *variant) Delete(ctx *gin.Context) {
	_, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	variantId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if err := c.serv.Delete(uint(variantId)); err != nil {
		ctx.JSON(http.StatusInternalServerError, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("VARIANT_DELETED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
