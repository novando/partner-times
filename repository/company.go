package repository

import (
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type CompanyRepository interface {
	Save(company *model.Company) error
	Get() []model.Company
	GetById(id uint) (model.Company, error)
	Delete(id uint) error
}

type company struct {
	db *gorm.DB
}

func NewCompanyRepository(db *gorm.DB) CompanyRepository {
	return &company{db: db}
}

func (r *company) Save(company *model.Company) error {
	return r.db.Save(&company).Error
}

func (r *company) Get() []model.Company {
	var companyData []model.Company
	r.db.Find(&companyData)
	return companyData
}

func (r *company) GetById(id uint) (model.Company, error) {
	var companyData model.Company
	err := r.db.First(&companyData, id).Error
	return companyData, err
}

func (r *company) Delete(id uint) error {
	return r.db.Delete(&model.Company{}, id).Error
}
