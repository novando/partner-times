package model

import "gorm.io/gorm"

type Ticket struct {
	gorm.Model
	CreatedBy      string
	UpdatedBy      string
	IdTicket       string `gorm:"not null;unique" binding:"required"`
	Phone          string
	Pic            string
	Price          float64
	VenueID        uint
	Venue          Venue        `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	VariantID      uint         `binding:"required"`
	Variant        Variant      `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	TicketStatusID uint         `gorm:"not null;default:1" binding:"required"`
	TicketStatus   TicketStatus `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
