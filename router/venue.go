package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func VenueRoute(r *gin.Engine, serv service.VenueService) {
	cont := controller.NewVenueController(serv)
	route := r.Group("/venue")

	route.GET("/company/:companyId", cont.GetByCompany)
	route.DELETE("/:id", cont.Delete)
	route.PUT("/:id", cont.Update)
	route.GET("/:id", cont.GetDetail)
	route.POST("", cont.Create)
	route.GET("", cont.GetAll)
}
