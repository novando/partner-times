package controller

import (
	"errors"
	"net/http"
	"strconv"

	arutek "github.com/arutek/backend-go-package"
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/service"
)

type company struct {
	serv service.CompanyService
}

func NewCompanyController(companyService service.CompanyService) *company {
	return &company{serv: companyService}
}

func (c *company) CreateCompany(ctx *gin.Context) {
	claim, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var reqBody dto.CompanyReq
	err = ctx.ShouldBind(&reqBody)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if uint(claim["roleId"].(float64)) > 2 {
		err = errors.New("UNAUTHORIZED")
		ctx.JSON(http.StatusUnauthorized, gin.H(arutek.Error(err, err.Error())))
		return
	}
	err, statusCode = c.serv.Create(&reqBody)
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("COMPANY_CREATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *company) FindAllCompanies(ctx *gin.Context) {
	claim, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var reqBody dto.CompanyReq
	err = ctx.ShouldBind(&reqBody)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if uint(claim["roleId"].(float64)) > 2 {
		err = errors.New("UNAUTHORIZED")
		ctx.JSON(http.StatusUnauthorized, gin.H(arutek.Error(err, err.Error())))
		return
	}
	msgVal := "DATA_FOUND"
	get := c.serv.Get()
	if len(get) < 1 {
		msgVal = "DATA_NOT_FOUND"
	}
	res := arutek.Response(msgVal, get, int64(len(get)))
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *company) FindByIdCompany(ctx *gin.Context) {
	claim, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if uint(claim["roleId"].(float64)) > 2 {
		err = errors.New("UNAUTHORIZED")
		ctx.JSON(http.StatusUnauthorized, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	get, err, statusCode := c.serv.GetDetail(uint(paramId))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("DATA_FOUND", get, 0)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *company) UpdateCompany(ctx *gin.Context) {
	claim, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if uint(claim["roleId"].(float64)) > 3 {
		err = errors.New("UNAUTHORIZED")
		ctx.JSON(http.StatusUnauthorized, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	var reqBody dto.CompanyEditReq
	err = ctx.ShouldBind(&reqBody)
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	err, statusCode = c.serv.Update(&reqBody, uint(paramId))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("COMPANY_UPDATED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}

func (c *company) DeleteCompany(ctx *gin.Context) {
	claim, statusCode, err := arutek.AuthMiddleware(ctx.GetHeader("Authorization"))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	if uint(claim["roleId"].(float64)) > 1 {
		err = errors.New("UNAUTHORIZED")
		ctx.JSON(http.StatusUnauthorized, gin.H(arutek.Error(err, err.Error())))
		return
	}
	paramId, err := strconv.Atoi(ctx.Param("id"))
	if err != nil {
		ctx.JSON(http.StatusBadRequest, gin.H(arutek.Error(err, err.Error())))
		return
	}
	err, statusCode = c.serv.Delete(uint(paramId))
	if err != nil {
		ctx.JSON(statusCode, gin.H(arutek.Error(err, err.Error())))
		return
	}
	res := arutek.Response("COMPANY_DELETED", nil, -1)
	ctx.JSON(http.StatusOK, gin.H(res))
}
