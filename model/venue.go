package model

import "gorm.io/gorm"

type Venue struct {
	gorm.Model
	CreatedBy string
	UpdatedBy string
	Name      string `gorm:"not null" binding:"required"`
	Owner     string `gorm:"not null" binding:"required"`
	Capacity  uint32 `gorm:"not null" binding:"required"`
	CompanyID uint
	Company   Company `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
