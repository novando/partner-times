package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func UserRoute(r *gin.Engine, serv service.UserService) {
	authController := controller.NewAuthController(serv)
	route := r.Group("/auth")

	route.POST("/register", authController.Register)
	route.POST("/login", authController.Login)
	route.GET("/me", authController.Me)
	route.GET("/profile", authController.Profile)
}
