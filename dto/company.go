package dto

type CompanyRes struct {
	ID        uint
	Name      string `json:"name"`
	Logo      string `json:"logo"`
	Owner     string `json:"owner"`
	Secretary string `json:"secretary"`
	Finance   string `json:"finance"`
	Province  string `json:"province"`
	City      string `json:"city"`
	Address   string `json:"address"`
	Phone     string `json:"phone"`
}

type CompanyReq struct {
	Name     string `json:"name" binding:"required"`
	Owner    string `json:"owner" binding:"required"`
	Province string `json:"province" binding:"required"`
	City     string `json:"city" binding:"required"`
	Phone    string `json:"phone" binding:"required"`
}

type CompanyEditReq struct {
	CompanyReq
	Secretary string `json:"secretary" binding:"required"`
	Finance   string `json:"finance" binding:"required"`
}
