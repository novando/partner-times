package repository

import (
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type UserRepository interface {
	GetByEmail(email string) (model.User, error)
	Save(user *model.User) error
	GetById(id uint) (model.User, error)
}

type user struct {
	db *gorm.DB
}

func NewUserRepository(db *gorm.DB) UserRepository {
	return &user{db: db}
}

func (r *user) GetByEmail(email string) (model.User, error) {
	var userData model.User
	err := r.db.Preload("Company").Preload("Role").Where("email = ?", email).First(&userData).Error
	return userData, err
}

func (r *user) Save(user *model.User) error {
	return r.db.Save(&user).Error
}

func (r *user) GetById(id uint) (model.User, error) {
	var userData model.User
	err := r.db.Preload("Company").Preload("Role").First(&userData, id).Error
	return userData, err
}
