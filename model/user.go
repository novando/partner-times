package model

import (
	"gorm.io/gorm"
)

type User struct {
	gorm.Model
	CreatedBy string
	UpdatedBy string
	Name      string `gorm:"not null" binding:"required"`
	Email     string `gorm:"not null;unique" binding:"required"`
	Password  string `gorm:"not null" binding:"required"`
	RoleID    uint   `gorm:"not null; default:1"`
	Role      Role   `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
	CompanyID uint
	Company   Company `gorm:"constraint:OnUpdate:CASCADE,OnDelete:SET NULL;"`
}
