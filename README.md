### Need Go version >= 1.20

This repository should be on a directory called `partner-times`

to run this app, you should put this directory, inside [Arutek Backend Core App](https://gitlab.com/arutek/backend/core-app) in subdirectory called `apps`. Than run the following Bash command in the Core App directory

```bash
go run cmd/partner-times/main.go
```