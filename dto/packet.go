package dto

type PacketRes struct {
	ID          uint   `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	CompanyID   uint   `json:"companyId"`
}

type PacketReq struct {
	PacketEditReq
	CompanyID uint `json:"companyId"`
}

type PacketEditReq struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description"`
}
