package model

import "gorm.io/gorm"

type Company struct {
	gorm.Model
	CreatedBy string
	UpdatedBy string
	Name      string `gorm:"not null" binding:"required"`
	Logo      string `gorm:"not null" binding:"required"`
	Owner     string `gorm:"not null" binding:"required"`
	Secretary string `gorm:"not null" binding:"required"`
	Finance   string `gorm:"not null" binding:"required"`
	Province  string `gorm:"not null" binding:"required"`
	City      string `gorm:"not null" binding:"required"`
	Address   string `gorm:"not null" binding:"required"`
	Phone     string `gorm:"not null" binding:"required"`
}
