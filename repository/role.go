package repository

import (
	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type RoleRepository interface {
	Get(page int, size int) []model.Role
	Count() int64
	GetById(id uint) (model.Role, error)
	Delete(id uint) error
	Save(roleData *model.Role) error
}

type role struct {
	db *gorm.DB
}

func NewRoleRepository(db *gorm.DB) RoleRepository {
	return &role{db: db}
}

func (r *role) Get(page int, size int) []model.Role {
	var roleData []model.Role
	r.db.Scopes(arutek.GormPaginate(page, size)).Find(&roleData)
	return roleData
}

func (r *role) Count() int64 {
	var countData int64
	r.db.Table("roles").Count(&countData)
	return countData
}

func (r *role) GetById(id uint) (model.Role, error) {
	var roleData model.Role
	err := r.db.First(&roleData, id).Error
	return roleData, err
}

func (r *role) Delete(id uint) error {
	return r.db.Delete(&model.Role{}, id).Error
}

func (r *role) Save(roleData *model.Role) error {
	return r.db.Save(roleData).Error
}
