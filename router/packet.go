package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func PacketRoute(r *gin.Engine, serv service.PacketService) {
	cont := controller.NewPacketController(serv)
	route := r.Group("/packet")

	route.DELETE("/:id", cont.Delete)
	route.PUT("/:id", cont.Update)
	route.GET("/:id", cont.GetDetail)
	route.POST("", cont.Create)
	route.GET("", cont.Get)
}
