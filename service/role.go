package service

import (
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type RoleService interface {
	Get(page int, size int) (roleDto []dto.RoleRes, count int64)
	GetById(roleId uint) (roleDto dto.RoleRes, err error)
	Create(roleDto *dto.RoleReq) error
	Update(roleId uint, roleDto *dto.RoleReq) error
	Delete(roleId uint) error
}

type role struct {
	repo repository.RoleRepository
}

func NewRoleService(rr repository.RoleRepository) RoleService {
	return &role{repo: rr}
}

func (s *role) Get(page int, size int) (roleDto []dto.RoleRes, count int64) {
	count = s.repo.Count()
	roleData := s.repo.Get(page, size)
	for _, val := range roleData {
		roleDto = append(roleDto, dto.RoleRes{
			ID:   val.ID,
			Name: val.Name,
		})
	}
	return
}

func (s *role) GetById(roleId uint) (roleDto dto.RoleRes, err error) {
	roleData, err := s.repo.GetById(roleId)
	roleDto.ID = roleData.ID
	roleDto.Name = roleData.Name
	return
}

func (s *role) Create(roleDto *dto.RoleReq) error {
	return s.repo.Save(&model.Role{
		Name: roleDto.Name,
	})
}

func (s *role) Update(roleId uint, roleDto *dto.RoleReq) error {
	roleData, err := s.repo.GetById(roleId)
	if err != nil {
		return err
	}
	roleData.Name = roleDto.Name
	return s.repo.Save(&roleData)
}

func (s *role) Delete(roleId uint) error {
	return s.repo.Delete(roleId)
}
