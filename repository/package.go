package repository

import (
	arutek "github.com/arutek/backend-go-package"
	"gitlab.com/arutek/backend/partner-times/model"
	"gorm.io/gorm"
)

type PackageRepository interface {
	GetByCompany(companyId uint, page int, size int) []model.Package
	CountByCompany(companyId uint) int64
	GetById(id uint) (model.Package, error)
	Save(packageData *model.Package) error
	Delete(id uint) error
}

type packageStruct struct {
	db *gorm.DB
}

func NewPackageRepository(db *gorm.DB) PackageRepository {
	return &packageStruct{db: db}
}

func (r *packageStruct) GetByCompany(companyId uint, page int, size int) []model.Package {
	var packageData []model.Package
	r.db.Where("company_id = ?", companyId).Scopes(arutek.GormPaginate(page, size)).Find(&packageData)
	return packageData
}

func (r *packageStruct) CountByCompany(companyId uint) int64 {
	var countData int64
	r.db.Table("packages").Where("company_id = ?", companyId).Count(&countData)
	return countData
}

func (r *packageStruct) GetById(id uint) (model.Package, error) {
	var packageData model.Package
	err := r.db.First(&packageData, id).Error
	return packageData, err
}

func (r *packageStruct) Save(packageData *model.Package) error {
	return r.db.Save(&packageData).Error
}

func (r *packageStruct) Delete(id uint) error {
	return r.db.Delete(&model.Package{}, id).Error
}
