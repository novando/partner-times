package router

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/arutek/backend/partner-times/controller"
	"gitlab.com/arutek/backend/partner-times/service"
)

func TicketRoute(r *gin.Engine, serv service.TicketService) {
	cont := controller.NewTicketController(serv)
	route := r.Group("/ticket")

	route.GET("/unfinished", cont.GetUnfinished)
	route.PUT("/:id", cont.Update)
	route.GET("/:id", cont.GetDetail)
	route.POST("", cont.Create)
	route.GET("", cont.Get)
}
