package service

import (
	"gitlab.com/arutek/backend/partner-times/dto"
	"gitlab.com/arutek/backend/partner-times/model"
	"gitlab.com/arutek/backend/partner-times/repository"
)

type PacketService interface {
	Get(companyId uint, page int, size int) (packetDto []dto.PacketRes, count int64)
	GetDetail(packetId uint) (packetDto dto.PacketRes, err error)
	Delete(packetId uint) error
	Update(packetDto *dto.PacketEditReq, packetId uint) error
	Create(packetDto *dto.PacketReq) error
}

type packet struct {
	repo repository.PackageRepository
}

func NewPacketSerivce(rp repository.PackageRepository) PacketService {
	return &packet{repo: rp}
}

func (s *packet) Get(companyId uint, page int, size int) (packetDto []dto.PacketRes, count int64) {
	count = s.repo.CountByCompany(companyId)
	packetData := s.repo.GetByCompany(companyId, page, size)
	for _, val := range packetData {
		packetDto = append(packetDto, dto.PacketRes{
			ID:          val.ID,
			Name:        val.Name,
			Description: val.Description,
			CompanyID:   val.CompanyID,
		})
	}
	return
}

func (s *packet) GetDetail(packetId uint) (packetDto dto.PacketRes, err error) {
	packetData, err := s.repo.GetById(packetId)
	if err != nil {
		return
	}
	packetDto = dto.PacketRes{
		ID:          packetData.ID,
		Name:        packetData.Name,
		Description: packetData.Description,
		CompanyID:   packetData.CompanyID,
	}
	return
}

func (s *packet) Delete(packetId uint) error {
	return s.repo.Delete(packetId)
}

func (s *packet) Update(packetDto *dto.PacketEditReq, packetId uint) error {
	packetData, err := s.repo.GetById(packetId)
	if err != nil {
		return err
	}
	packetData.Name = packetDto.Name
	packetData.Description = packetDto.Description
	return s.repo.Save(&packetData)
}

func (s *packet) Create(packetDto *dto.PacketReq) error {
	return s.repo.Save(&model.Package{
		Name:        packetDto.Name,
		Description: packetDto.Description,
		CompanyID:   packetDto.CompanyID,
	})
}
