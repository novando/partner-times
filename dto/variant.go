package dto

type VariantRes struct {
	ID          uint    `json:"id"`
	Name        string  `json:"name"`
	MaxCapacity uint16  `json:"maxCapacity"`
	Duration    uint16  `json:"duration"`
	Price       float64 `json:"price"`
	Days        []uint8 `json:"days"`
}

type VariantReq struct {
	Name        string  `json:"name"`
	MaxCapacity uint16  `json:"maxCapacity"`
	Duration    uint16  `json:"duration"`
	Price       float64 `json:"price"`
	PackageID   uint    `json:"pacakgeId"`
}

type VariantEditReq struct {
	Name string  `json:"name"`
	Days []uint8 `json:"days"`
}
